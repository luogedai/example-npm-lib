# example-npm-lib

> one-liner description of the module

background details relevant to understanding what this module does

## Usage

```js
var exampleNpmLib = require('example-npm-lib')

console.log('hello warld')
```

outputs

```
hello warld
```

## API

```js
var exampleNpmLib = require('example-npm-lib')
```

See [api_formatting.md](api_formatting.md) for tips.

## Install

With [npm](https://npmjs.org/) installed, run

```
$ npm install example-npm-lib
```

## Acknowledgments

example-npm-lib was inspired by..

## See Also

- [`noffle/common-readme`](https://github.com/noffle/common-readme)
- ...

## License

ISC

